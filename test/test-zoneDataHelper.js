/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

function testProperties() {
   assert((zoneDataHelper.Zones != null) , "Zones property is null")
   var propType = typeof zoneDataHelper.Zones
   assert((propType == "object") , String.format("Zones property is not 'object', it is {0}", propType))
   assert((!Array.isArray(zoneDataHelper.Zones)) , "Zones is array, it should NOT be")
   //
   assert((zoneDataHelper.Rules != null) , "Rules property is null")
   var propType = typeof zoneDataHelper.Rules
   assert((propType == "object") , String.format("Rules property is not 'object', it is {0}", propType))   
   assert((!Array.isArray(zoneDataHelper.Rules)) , "Rules is array, it should NOT be")
   //
   assert((zoneDataHelper.RegionList != null) , "RegionList property is null")
   var propType = typeof zoneDataHelper.RegionList
   assert((propType == "object") , String.format("RegionList property is not 'object', it is {0}", propType))      
   assert((Array.isArray(zoneDataHelper.RegionList)) , "RegionList is not array, it should be")
   //
   assert((zoneDataHelper.RegionToCityMap != null) , "RegionList property is null")
   var propType = typeof zoneDataHelper.RegionToCityMap
   assert((propType == "object") , String.format("RegionToCityMap property is not 'object', it is {0}", propType))      
   assert((!Array.isArray(zoneDataHelper.RegionToCityMap)) , "RegionToCityMap is array, it should NOT be")
   //
   assert((zoneDataHelper.NO_REGION_NAME != null) , "RegionList property is null")
   var propType = typeof zoneDataHelper.NO_REGION_NAME
   assert((propType == "string") , String.format("NO_REGION_NAME property is not 'object', it is {0}", propType))
   assert(zoneDataHelper.NO_REGION_NAME != "" , "NO_REGION_NAME is empty string, it should NOT")      
   
   logInfo("testProperties -> OK")
}

function testSubsetAndJoin() {
   var zone1 = "Europe/Sofia"
   var zone2 = "US/Pacific"
   
   logInfo("Extracting two zones in one subset")
   var subset = zoneDataHelper.getSubset([zone1, zone2])
   
   logInfo("Extracting two zones, each in separate subset")
   var europeZone = zoneDataHelper.getSubset([zone1])
   var pacificZone = zoneDataHelper.getSubset([zone2])
   
   logInfo("Joining sets")
   var joined = zoneDataHelper.joinSubsets(europeZone, pacificZone)
   
   logInfo("Joining sets which contain - they have same data")
   var lastSet = zoneDataHelper.joinSubsets(subset, joined)
  
   logInfo("Comparing result sets")
   assertEquals (joined, lastSet)
   assertEquals (joined, subset)
   
   logInfo("testSubsetAndJoin -> OK")
}

function testGetZoneName() {
   logInfo('Validate "Europe", "Sofia"')
   var zoneName = zoneDataHelper.getZoneName("Europe", "Sofia")
   assertEquals(zoneName, "Europe/Sofia")
   
   logInfo('Validate "no region", "Turkey"')
   zoneName = zoneDataHelper.getZoneName(zoneDataHelper.NO_REGION_NAME, "Turkey")
   assertEquals(zoneName, "Turkey")
   
   logInfo('Validate empty string result for unexisting region')
   zoneName = zoneDataHelper.getZoneName("Nono", "Sofia")
   assertEquals(zoneName, "")
   
   logInfo('Validate empty string result for unexisting region')
   zoneName = zoneDataHelper.getZoneName("Europe", "Nono")
   assertEquals(zoneName, "")
   
   logInfo("testGetZoneName -> OK")
}

function testParseZoneName() {
   logInfo('Validate "Europe/Sofia"')
   var obj = zoneDataHelper.parseZoneName("Europe/Sofia")
   assertEquals({regionName: "Europe", cityName : "Sofia"}, obj)
   
   logInfo('Validate "Turkey"')
   var obj = zoneDataHelper.parseZoneName("Turkey")
   assertEquals({regionName: zoneDataHelper.NO_REGION_NAME, cityName : "Turkey"}, obj)

   logInfo('Validate with non-existing region')
   var obj = zoneDataHelper.parseZoneName("No/Sofia")
   assertEquals(null, obj)   
   
   logInfo('Validate with non-existing city')
   var obj = zoneDataHelper.parseZoneName("Europe/No")
   assertEquals(null, obj)   
   
   logInfo("testParseZoneName -> OK")
}

function startTests() {
   logInfo("tests started")
   
   zoneDataHelper.loadData(zonesData)
   testProperties()
   logInfo("---------------------------------------------")
   testSubsetAndJoin()
   logInfo("---------------------------------------------")
   testGetZoneName()
   logInfo("---------------------------------------------")
   testParseZoneName()
   
   logInfo("tests ended")
}