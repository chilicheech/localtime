/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */
 
function AssertError(message) {
    this.message = message || "Assertion error occurred";
}
AssertError.prototype = new Error();
AssertError.prototype.constructor = AssertError;

function assert(condition, message) {
   if (condition != true) {
      throw new AssertError(message)
   }
}

function arrayContains(array, value) {
   for (var i=0; i < array.length; i++) {
      if (array[i] == value) {
         return true
      }
   }
   return false
}

function assertEquals(value1, value2, path) {
   if (typeof path === "undefined") {
      path = "this"
   }
   var type1 = (typeof value1)
   var type2 = (typeof value2)
   
   if (type1 != type2) {
      assert(false, String.format("assertion failed -> [{2}] type of objects are different, expected type '{0}', actual type '{1}'", type1, type2, path))
   }
   
   if (type1 == "undefined") {
      assert(false, "value1 is undefined")
   }
   
   if (type1 == "boolean" || type1 == "number" || type1 == "string") {
      if (value1 != value2) {
         assert(false, String.format("assertion failed -> [{2}] values are different, expected '{0}', actual '{1}'", value1, value2, path))
      }
   } else if (type1 == "object") {
      var isArray1 = Array.isArray(value1)
      var isArray2 = Array.isArray(value2)
      
      if (isArray1 && !isArray2) {
         assert(false, String.format("assertion failed -> [{0}] expected object is array, but the actual is not", path))
      } else if (!isArray1 && isArray2) {
         assert(false, String.format("assertion failed -> [{0}] expected object is not array, but actual is", path))
      }
      
      if (!isArray1) {
         var checkedProperties = []
         for (var prop in value1) {
            var newPath = path + "." + prop
            checkedProperties.push(prop)
            assertEquals(value1[prop], value2[prop], newPath)
         }
         
         for (var prop in value2) {
            if (!arrayContains(checkedProperties, prop)) {
               assert(false, String.format("assertion failed -> [{0}] actual object has property not found in the expected -> '{1}'", path, prop))
            }
         }
      } else {
         // comparing arrays expecting that their elements are equal
         if (value1.length != value2.length) {
            assert(false, String.format("assertion failed -> [{2}] lengthts are different, expected '{0}', actual '{1}'", value1.length, value2.length, path))
         }
         for (var prop in value1) {
            var newPath = path + "[" + prop + "]"
            assertEquals(value1[prop], value2[prop], newPath)
         }
      }
   }
}

function assertDictAreEquals(expectedDict, actualDict) {
   for( var key in expectedDict) {
      var assertMsg = "found diff in key '" + key + "' exoected '" + expectedDict[key] + "' actual '" + actualDict[key] + "'"
      if (!assert( (expectedDict[key] == actualDict[key]), assertMsg)) {
         break
      }
   }
}