/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

 function getLogControl() {
   return document.getElementById("outputPre")
}

_logLabels = [
   "ERROR   ",
   "WARNING ",
   "INFO    ",
   "DEBUG   ",
   "TRACE   "
]

function logInfo() {
   _log(2, arguments)
}

function logError() {
   _log(0, arguments)
}

function logWarning() {
   _log(1, arguments)
}

function logDebug() {
   _log(3, arguments)
}

function logTrace() {
   _log(4, arguments)
}

function logIndentIncrease() {
   indent += 1
}

function logIndentDecrease() {
   indent -= 1
   if (indent < 1) {
      indent = 0
   }
}

logLevel = 4
indent = 1
indentPrefix = "    "

function getTimestamp() {
   var dt = new Date()
   var result = String.format("{0}-{1}-{2}T{3}:{4}:{5}:{6}",
      dt.getFullYear(),
      pad((dt.getMonth()+1).toString()),
      pad(dt.getDay().toString()),
      pad(dt.getHours().toString()),
      pad(dt.getMinutes().toString()),
      pad(dt.getSeconds().toString()),
      pad(dt.getMilliseconds().toString(), 3))
   
   return result
}

function _log(level, args) {
   var ts = getTimestamp()
   
   if (level > logLevel) {
      return
   }
   
   var label = _logLabels[level]
   var logger = getLogControl()
   var msg = ""
   for (var i in args) {
      msg = msg + args[i]
   }
   msg += "\n"
   var localIndent = new Array(indent).join(indentPrefix)
   msg = ts + " " + label + localIndent + msg
   logger.textContent += msg
}

function log_dict(dict, label) {
   if (label) {
      logInfo(label)
   }
   for (var prop in dict) {
      var msg = "  " + prop + " : " + dict[prop]
      logInfo(msg)
   }
}