/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

 
function ConfigSet(param1, param2) {
   if (typeof param1 == "string") {
      if (param2 instanceof ConfigSet) {
         var name = param1
         var source = param2
         this.Name = name
         this.UrlRegex = source.UrlRegex
         this.DateMask = source.DateMask
         this.OutputFormat = source.OutputFormat
         this.Timezone = source.Timezone
         this.IsActive = source.IsActive
         this.IsValid = source.IsValid
      } else {
         var name = param1
         this.Name = name
      }
   } else {
      var storageObject = param1
      this.Name = storageObject.Name == null ? this.Name : storageObject.Name
      this.UrlRegex = storageObject.UrlRegex == null ? this.UrlRegex : storageObject.UrlRegex
      this.DateMask = storageObject.DateMask == null ? this.DateMask : storageObject.DateMask
      this.OutputFormat = storageObject.OutputFormat == null ? this.OutputFormat : storageObject.OutputFormat
      this.Timezone = storageObject.Timezone == null ? this.Timezone : storageObject.Timezone
      this.IsActive = storageObject.IsActive == null ? this.IsActive : storageObject.IsActive
      this.IsValid = storageObject.IsValid == null ? this.IsValid : storageObject.IsValid
   }
}

ConfigSet.prototype = (function(){
   return {
      Name : "",
      UrlRegex : [],
      DateMask : [],
      OutputFormat : "yyyy-mm-dd HH:MM",
      Timezone : "",
      IsActive : true,
      IsValid : false,
      
      // methods
      getUrlRegexAsText : function() {
         return util.array.arrayAsText(this.UrlRegex)
      },
      
      setUrlRegexAsText : function(value) {
         this.UrlRegex = util.array.parseTextToArray(value)
      },
      
      getDateMaskAsText : function() {
         return util.array.arrayAsText(this.DateMask)
      },
      
      setDateMaskAsText : function(value) {
         this.DateMask = util.array.parseTextToArray(value)
      },
      
      toString : function() {
         return this.Name
      },
      
      getDisplayText : function() {
         var result = this.Name
         if (!this.IsValid) {
            result = result + ' *'
         }
         return result
      }
   } // return
})()