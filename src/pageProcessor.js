/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

function PageProcessor(dateMatcherList, timeZone, localTimeZone, outputFormat) {
   this._dateMatcherList = dateMatcherList
   this._timeZone = timeZone
   this._localTimeZone = localTimeZone
   this._outputFormat = outputFormat
   this._modificationCount = 0
   this._pendingModifications = []
   this._document = null
   this._lastConvertCouple = null
}

PageProcessor.prototype = {
   // constants
   CLASS_LocalTimeOriginal : "localTimeOriginal",
   CLASS_LocalTimeNew : "localTimeNew",

   getModificationCount : function() {
      return this._modificationCount
   },

   _prepareElement : function(textContent, clazz, visible, title) {
      var document = this._document

      var element = document.createElement('span')
      element.setAttribute('class', clazz)
      
      if (title != null) {
         element.setAttribute('title', title)
      }

      if (visible) {
         element.style.display = "inline"
      } else {
         element.style.display = "none"
      }

      textNode = document.createTextNode(textContent)
      element.appendChild(textNode)

      return element
   },

   processPage : function(document) {
      this._document = document
      this.traverseElement(document)

      for (var i=0; i < this._pendingModifications.length; i++) {
         var mod = this._pendingModifications[i]

         var originalElement = this._prepareElement(mod.originalContent, PageProcessor.prototype.CLASS_LocalTimeOriginal , false, mod.newContent.trim())
         var newElement = this._prepareElement(mod.newContent, PageProcessor.prototype.CLASS_LocalTimeNew, true, mod.originalContent.trim())

         if (mod.element.previousSibling) {
            mod.element.previousSibling.insertAdjacentElement("afterEnd", originalElement)
            mod.element.previousSibling.insertAdjacentElement("afterEnd", newElement)
            mod.element.parentNode.removeChild(mod.element)
         } else {
            mod.element.parentElement.appendChild(originalElement)
            mod.element.parentElement.appendChild(newElement)
            mod.element.parentElement.removeChild(mod.element)
         }
      }
   },

   traverseElement : function(element) {
      if (element.nodeType == Element.prototype.TEXT_NODE) {
         if (element.textContent) {
            this.processElement(element)
         }
      } else {
         if (element.childNodes) {
            var childNodes = element.childNodes
            for (var i=0; i < childNodes.length; i++) {
               this.traverseElement(childNodes[i])
            }
         }
      }
   },

   convertDate : function(year, month, day, hours, minutes, seconds, milliseconds) {
      var dt = new timezoneJS.Date(year, month-1, day, hours, minutes, seconds, this._timeZone)
      dt.setMilliseconds(milliseconds)
      dt.setTimezone(this._localTimeZone)
      
      var convertedDate = dateFormat(dt, this._outputFormat)
      
      return convertedDate
   },

   processElement : function(element) {
      if (element.parentElement == null || element.parentElement.className == PageProcessor.CLASS_LocalTimeOriginal || element.parentElement.className == PageProcessor.CLASS_LocalTimeNew) {
         return;
      }
      
      var modificationsDone = 0
      
      for (var i = 0; i < this._dateMatcherList.length; i++) {
         var originalContent = element.textContent
         var newContent = DateParser.matchDate(element.textContent, this._dateMatcherList[i], this.convertDate.bind(this))

         if (newContent != originalContent) {
            modificationsDone += 1

            this._pendingModifications.push({
               element : element,
               newContent : newContent,
               originalContent: originalContent
            })
         }
      }

      this._modificationCount += modificationsDone
   },

   _setElementVisible : function(element, visible) {
      if (visible == true) {
         element.style.display = "inline"
      } else {
         element.style.display = "none"
      }
   },

   swapVisibleClass : function(showOriginal) {
         var newElements = this._document.getElementsByClassName(PageProcessor.prototype.CLASS_LocalTimeNew)
         var originalElements = this._document.getElementsByClassName(PageProcessor.prototype.CLASS_LocalTimeOriginal)

         var newElementVisible = null
         if (showOriginal) {
            newElementVisible = false
         } else {
            newElementVisible = true
         }

         for(var i=0; i < newElements.length; i++) {
            this._setElementVisible(newElements[i], newElementVisible)
         }
         for(var i=0; i < originalElements.length; i++) {
            this._setElementVisible(originalElements[i], !newElementVisible)
         }
   }
} // pageProcessor