/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */
 
 // this variable will hold the state of the tab
var tabState = {
   matchedSite : false,
   matchCount : 0,
   isOriginalVisible : true
}

function updateBadgeText(configSettingFound, modificationCount) {
   if (configSettingFound) {
      tabState.matchedSite = true
      tabState.matchCount = modificationCount
      tabState.isOriginalVisible = false
   } else {
      tabState.matchedSite = false
      tabState.matchCount = 0
      tabState.isOriginalVisible = true
   }
   
   sendPageProcessingCompleted()
}

function sendPageProcessingCompleted() {
   chrome.extension.sendMessage(
      {
         type: "pageProcessingCompleted", 
         value: tabState
      }
   )
}

chrome.extension.onMessage.addListener(
   function(msg, sender, sendResponse) {
      if (msg.type == "swapVisibility") {
         if (pageProcessor) {
            tabState.isOriginalVisible = ! tabState.isOriginalVisible
            pageProcessor.swapVisibleClass(tabState.isOriginalVisible)
         }
         sendPageProcessingCompleted()
      }
   }
);

function main(settings) {
   if (settings == null) {
      return
   }
   
   // loading zone subset
   timezoneJS.timezone.loadPreparsedData(settings.zoneSubset)

   if (document.readyState == "complete") {
      pageProcessor = new PageProcessor(settings.dateMatcherList, settings.timeZone, settings.localTimezone, settings.outputFormat)
      pageProcessor.processPage(document)
      updateBadgeText(true, pageProcessor.getModificationCount())
   } else {
      var callback = function() {
         if (document.readyState == "complete") {
            pageProcessor = new PageProcessor(settings.dateMatcherList, settings.timeZone, settings.localTimezone, settings.outputFormat)
            pageProcessor.processPage(document)
            updateBadgeText(true, pageProcessor.getModificationCount())
         }
      }
      document.addEventListener('readystatechange', callback, false);
   }
}

function doWork(localTimezone, configSetList, tzDb) {
   // if one of the settings is not save
   // there's nothing to do
   if (localTimezone == null) {
      return
   }
   
   if (tzDb == null) {
      return
   }
   
   if (configSetList == null) {
      return
   }
   
   var url = document.URL
   var foundConfigSet = null
   for(var i=0; i < configSetList.length; i++) {
      var configSet = new ConfigSet(configSetList[i])
      
      if (!configSet.IsActive || !configSet.IsValid) {
         continue
      }
      
      var regexList = configSet.UrlRegex
      for(var j=0; j < regexList.length; j++) {
         var regex = new RegExp(regexList[j], "igm")
         if (url.search(regex) != -1) {
            foundConfigSet = configSet
            break
         }
      }
      
      if (foundConfigSet != null) {
         break;
      }
   }

   if (foundConfigSet != null) {
      var result = {
         outputFormat : foundConfigSet.OutputFormat,
         regexList : [],
         timeZone : foundConfigSet.Timezone,
         dateMatcherList : [],
         localTimezone: localTimezone,
         zoneSubset: tzDb
      }
      
      var dateMaskList = foundConfigSet.DateMask
      for (var i=0; i < dateMaskList.length; i++) {
         var mask = dateMaskList[i]
         var dateMatcher = DateParser.getDateParserMatcher(mask)
         result.dateMatcherList.push(dateMatcher)
      }
      
      if (result.dateMatcherList.length > 0) {
         main(result)
      } else {
         updateBadgeText(false, 0);
      }
   } else {
      updateBadgeText(false, 0);
   }
}   

settings.load(doWork)