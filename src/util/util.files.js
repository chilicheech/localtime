/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

var util = (function(util){
   util.files = {
      readTextFile : function(file, onLoaded) {
         var fileReader = new FileReader()
         fileReader.onload = function(onloadEvent) {
            if (typeof onLoaded == "function") {
               onLoaded(onloadEvent.target.result)
            }
         }
         fileReader.readAsText(file)         
      },
      
      downloadFile : function(content) {
         var base64encoded = btoa(content)
         window.open("data:application/octet-stream;base64,"+base64encoded, '_blank', '');
      }
   }
   
   return util;
}(util || {}))   