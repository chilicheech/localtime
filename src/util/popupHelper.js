/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

popupHelper = (function(document, window){
   var _dimmerDiv = null,
       _popupDiv = null,
       _closeDiv = null;
       
   function _hidePopup() {
      _dimmerDiv.style.visibility = "hidden"
      _popupDiv.style.visibility = "hidden"
      _closeDiv.style.visibility = "hidden"
      
      document.removeEventListener('keydown', _onKeyDownHandler, false)
   }
   
   function _activeElementIsChildOfPopupDiv() {
      if (document.activeElement == null) {
         return false;
      }
      
      var currentElement = document.activeElement;
      while (currentElement != null && currentElement != document.body) {
         if (currentElement.parentElement == _popupDiv) {
            return true
         }
         
         currentElement = currentElement.parentElement
      }
      
      return false
   }

   function _onKeyDownHandler(event){
      if (_dimmerDiv.style.visibility == "visible") {
         if (!_activeElementIsChildOfPopupDiv()) {
            _hidePopup();
            event.preventDefault()
         }
      }
   }

   function _createDimmer() {
      _dimmerDiv = document.createElement('div')
      _dimmerDiv.id = "options_popup_dimmer"
      
      _dimmerDiv.style.position = "absolute"
      _dimmerDiv.style.top = "0px"
      _dimmerDiv.style.left = "0px"
      
      _dimmerDiv.style.zIndex = "99"
      
      _dimmerDiv.style.display = "block"
      _dimmerDiv.style.visibility = "hidden"
      
      _dimmerDiv.style.opacity = 0.5
      _dimmerDiv.style.backgroundColor = "black"
      
      _dimmerDiv.onclick = _hidePopup
      document.body.appendChild(_dimmerDiv)
   }
   
   function _createCloseButton() {
      _closeDiv = document.createElement('div')
      _closeDiv.id = "options_popup_close"
      _closeDiv.style.width = "20px"
      _closeDiv.style.height = "20px"
      _closeDiv.style.position = "absolute"
      _closeDiv.visibility = "hidden"
      _closeDiv.onclick = _hidePopup
      _closeDiv.style.display = "block"
      _closeDiv.style.textAlign = "center"
      _closeDiv.style.verticalAlign = "middle"
      _closeDiv.style.padding = 0
      _closeDiv.style.margin = 0
      _closeDiv.style.fontSize = "14px"
      _closeDiv.style.cursor="pointer"
      
      var _textX = document.createTextNode("X")
      _closeDiv.appendChild(_textX)
      
      document.body.appendChild(_closeDiv)
   }
   
   function _initDimmer() {
      if (_dimmerDiv == null) {
         _dimmerDiv = document.getElementById("options_popup_dimmer")
         
         if (_dimmerDiv == null) {
            _createDimmer()
         }
      }
      
      if (_closeDiv == null) {
         _closeDiv = document.getElementById("options_popup_close")
         
         if (_closeDiv == null) {
            _createCloseButton()
         }
      }
   }
   
   function _popup() {
      document.addEventListener('keydown', _onKeyDownHandler, false)
      
      _dimmerDiv.style.width = document.body.scrollWidth + "px"
      _dimmerDiv.style.height = document.body.scrollHeight + "px"
      
      _dimmerDiv.style.visibility = "visible"
      _popupDiv.style.visibility = "visible"
      _closeDiv.style.visibility = "visible"
   }
   
   function _preparePopupDiv() {
      _popupDiv.style.zIndex = _dimmerDiv.style.zIndex + 1
      _popupDiv.style.position = "absolute"
      _popupDiv.style.display = "block"
      var left = (document.body.clientWidth - _popupDiv.offsetWidth) / 2
      var top = (document.body.clientHeight - _popupDiv.offsetHeight) / 2
      _popupDiv.style.left = left + "px"
      _popupDiv.style.top = top + "px"
      
      _closeDiv.style.zIndex = _popupDiv.style.zIndex + 1
      _closeDiv.style.left = left + _popupDiv.offsetWidth - 20
      _closeDiv.style.top = top 
   }
   
   return {
      popupDiv: function(div) {
         _popupDiv = div
         
         _initDimmer()   
         _preparePopupDiv()
         _popup()
      },
      
      hide : function() {
         _hidePopup()
      }
   }
}(document, window))