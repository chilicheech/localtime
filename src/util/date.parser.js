/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

function DateParser() {
}

function DateMatcher(regex, positions) {
   this.regex = regex;
   this.positions = positions;
}

DateParser = {
   monthNames : ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
   monthShortNames : ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
   tokens : {
      d:    {type: "day",     regex : "(3[0-1]|[1-2][0-9]|[1-9])(?:st|nd|d|th|rd)?", regexOptions : null},
      dd:   {type: "day",     regex : "(3[0-1]|[1-2][0-9]|0[1-9])(?:st|nd|d|th|rd)?", regexOptions : null},
      ddd:  {type: "weekday",     regex : "(Sun|Mon|Tue|Wed|Thu|Fri|Sat)", regexOptions : "i"},
      dddd: {type: "weekday",     regex : "(Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday)", regexOptions : "i"},
      m:    {type: "month",   regex : "(1[0-2]|[1-9])", regexOptions : null},
      mm:   {type: "month",   regex : "(0[1-9]|1[0-2])", regexOptions : null},
      mmm:  {type: "month",   regex : "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)", regexOptions : "i"},
      mmmm: {type: "month",   regex : "(January|February|March|April|May|June|July|August|September|October|November|December)", regexOptions : "i"},
      yy:   {type: "year",    regex : "(\\d{2})", regexOptions : null},
      yyyy: {type: "year",    regex : "(\\d{4})", regexOptions : null},
      h:    {type: "hours12", regex : "(1[0-2]|[1-9])", regexOptions : null},
      hh:   {type: "hours12", regex : "(0[1-9]|1[0-2])", regexOptions : null},
      H:    {type: "hours24", regex : "(2[0-3]|1[0-9]|[0-9])", regexOptions : null},
      HH:   {type: "hours24", regex : "(2[0-3]|1[0-9]|0[0-9])", regexOptions : null},
      M:    {type: "minutes",  regex : "([1-5][0-9]|[0-9])", regexOptions : null},
      MM:   {type: "minutes",  regex : "([1-5][0-9]|0[0-9])", regexOptions : null},
      s:    {type: "seconds", regex : "([1-5][0-9]|[0-9])", regexOptions : null},
      ss:   {type: "seconds", regex : "([1-5][0-9]|0[0-9])", regexOptions : null},
      t:    {type: "period",  regex : "(a|p)", regexOptions : "i"},
      tt:   {type: "period",  regex : "(am|pm|a\.m\.|p\.m\.)", regexOptions : "i"},
      T:    {type: "period",  regex : "(a|p)", regexOptions : "i"},
      TT:   {type: "period",  regex : "(am|pm|a\.m\.|p\.m\.)", regexOptions : "i"},
      l:    {type: "milliseconds", regex : "(\\d{1,3})", regexOptions : null },
      lll:    {type: "milliseconds", regex : "(\\d{3})", regexOptions : null },
      llll:    {type: "milliseconds", regex : "(0\\d{3})", regexOptions : null },
      ' ' :    {type: "space", regex : "(?:\\s|&nbsp;)", regexOptions : null }
      
   },
   
   tokenRegEx : /d{1,4}|m{1,4}|yy(?:yy)?|l{3,4}|([HhMsTtl])\1?|"[^"]*"|'[^']*'|[ ]/g,
   
   getDateParserMatcher : function(mask) {
      var position = 0
      
      var datePartPositions = {
         year: -1,
         month: -1,
         day: -1,
         hours24: -1,
         hours12: -1,
         period: -1,
         minutes: -1,
         seconds: -1,
         weekday: -1,
         milliseconds: -1
      }   
      
      // build regex string and object with date part positions
      var result = null
      try {
         var regexOptions = "g"
         var regexString = mask.replace(DateParser.tokenRegEx, function(match){
            var token = DateParser.tokens[match]
            if (token == null) {
               // string enclosed with single or double quotes
               return match.slice(1,match.length-1)
            } else {
               if (token.type != "space") {
                  if (token.regexOptions != null) {
                     if (regexOptions.indexOf(token.regexOptions) == -1) {
                        regexOptions += token.regexOptions
                     }
                  }
                  datePartPositions[token.type] = position
                  position += 1
               }
               
               return token.regex
            }
         })
         
         if (position >= 2) {
            // valid date mask should contain at least two format specifiers
            result = new DateMatcher(new RegExp(regexString, regexOptions), datePartPositions)
         }
      } catch (ex) {
         // something went wrong during DateMatcher creation
         result = null
      }
      return result
   },
   
   matchDate : function(value, dateMatcher, callback) {
      var positions = dateMatcher.positions
      return value.replace(dateMatcher.regex, function(){
      
         var args = []
         for (var i=1; i < arguments.length-2; i++) {
            args.push(arguments[i])
         }
         
         var now = new Date()
         
         var dateArguments = {
            year : positions.year != -1 ? parseInt(args[positions.year]) : now.getFullYear(),
            month : positions.month != -1 ? args[positions.month] : now.getMonth() + 1,
            day : positions.day != -1 ? parseInt(args[positions.day]) : now.getDate(),
            hours : 0,
            minutes : positions.minutes != -1 ? parseInt(args[positions.minutes]) : 0,
            seconds : positions.seconds != -1 ? parseInt(args[positions.seconds]) : 0,
            milliseconds : positions.milliseconds != -1 ? parseInt(args[positions.milliseconds]) : 0
         }
         
         var monthIndex = DateParser.monthNames.indexOf(dateArguments.month)
         if (monthIndex != -1) {
            dateArguments.month = monthIndex + 1
         } else {
            var monthIndex = DateParser.monthShortNames.indexOf(dateArguments.month)
            if (monthIndex != -1) {
               dateArguments.month = monthIndex + 1
            }
         }
         if (monthIndex == -1) {
            dateArguments.month = parseInt(dateArguments.month)   
         }
         
         if (positions.hours24 != -1) {
            dateArguments.hours = parseInt(args[positions.hours24])
         } else if (positions.hours12 != -1) {
            dateArguments.hours = parseInt(args[positions.hours12]) % 12
            if (positions.period != -1) {
               var periodMatch = args[positions.period].toLowerCase()
               if (periodMatch.search('p') != -1) {
                  dateArguments.hours = dateArguments.hours + 12
               }
            }
         }
         
         var result = callback(dateArguments.year, dateArguments.month, dateArguments.day, dateArguments.hours, dateArguments.minutes, dateArguments.seconds, dateArguments.milliseconds)
         return result
      })
   }
} // DataParser.prototype