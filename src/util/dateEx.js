/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

timezoneJS.timezone.loadPreparsedData = function(json) {
   timezoneJS.timezone.loadingScheme = timezoneJS.timezone.loadingSchemes.MANUAL_LOAD;

   timezoneJS.timezone.zones = {}
   timezoneJS.timezone.rules = {}
   for (var z in json.zones) {
      timezoneJS.timezone.zones[z] = json.zones[z];
   }
   for (var r in json.rules) {
      timezoneJS.timezone.rules[r] = json.rules[r];
   }   
}