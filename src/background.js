/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

var TITLE_nothing_matched = "Nothing to do..."
var TITLE_OriginalIsVisible = "Show converted time stamps"
var TITLE_OriginalIsNotVisible = "Show original time stamps"
var COLOR_nothing_matched = "#1BE035"
var COLOR_OriginalIsVisible = "#F03C3C"
var COLOR_OriginalIsNotVisible = "#1BE035"

function getBrowserActionCustomization(tabState) {
   var result = {
      title : TITLE_nothing_matched,
      color : COLOR_nothing_matched,
      text : "0"
   }
   if (tabState.matchCount > 0) {
      result.text = tabState.matchCount.toString()
      if (tabState.isOriginalVisible) {
         result.title = TITLE_OriginalIsVisible
         result.color = COLOR_OriginalIsVisible
      } else {
         result.title = TITLE_OriginalIsNotVisible
         result.color = COLOR_OriginalIsNotVisible
      }
   }
   return result
}

function updateTabState(tabState, tabId) {
   if (tabState.matchedSite == false) {
      chrome.browserAction.setTitle({title : "", tabId: tabId})
      chrome.browserAction.disable(tabId)
   } else {
      var browserActionCustomization = getBrowserActionCustomization(tabState)
      chrome.browserAction.enable(tabId)
      chrome.browserAction.setBadgeText({text : browserActionCustomization.text, tabId: tabId})
      chrome.browserAction.setTitle({title : browserActionCustomization.title, tabId: tabId})
      chrome.browserAction.setBadgeBackgroundColor({color : browserActionCustomization.color, tabId: tabId})
   }
}

// add change badge listener
chrome.extension.onMessage.addListener(
   function(msg, sender, sendResponse) {
      if (msg.type == "pageProcessingCompleted") {
         updateTabState(msg.value, sender.tab.id)
      }
   }
);

chrome.browserAction.onClicked.addListener(function(tab) {
   chrome.tabs.sendMessage(tab.id, {type: "swapVisibility"})
});